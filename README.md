# Description
This repository contains the pipeline files to test ulfh-mmc kernel.

This repository is based upon aroxell/linux-next-pipeline.

# Dependencies
aroxell/common-pipeline-stuff that needs to be included into gitlab-ci.yml file
and also a few local files like builds.yml tests.yml, sanity_job.yml and
tuxconfig.yml

# Usage
The goal is to only change the builds.yml, tests.yml, sanity_job.yml and
tuxconfig.yml files to sure your needs.
